import React from 'react'

function ShippingMethods({product}) {
    return (
        <>
            <div className="row">
                <div className="col-8">
                    <p className="ml-2">{product.name}</p>
                </div>
                <div className="col-4">
                    <p className="text-center">${product.price}</p>
                </div>
            </div>
        </>
    )
}

export default ShippingMethods
