import React from 'react'

function PaymentMethods(props) {      

    return (
        <>                     
            <button 
                type="button"                 
                className={`btn ${props.tab.status? "btn-primary " : "btn-outline-dark "} mb-2 col-md-3 col-sm-12`} 
                onClick = {()=>props.handleActive(props.tab)}
                >
                {props.tab.card}                
            </button>
        </>
    )
}

export default PaymentMethods
