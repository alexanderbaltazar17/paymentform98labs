import React from 'react'
import {Modal,Button} from "react-bootstrap"
function ValidationModal(props) {
    return (
        <>                    
        <Modal
            show={props.showMessage.status}
            onHide={()=>props.setShowMessage({...props.showMessage,status : false})}
            backdrop="static"
            keyboard={false}
        >

            <Modal.Header closeButton>
            <Modal.Title>{props.showMessage.title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {props.showMessage.message}
            </Modal.Body>

            <Modal.Footer>
            <Button variant="secondary" onClick={()=>props.setShowMessage({...props.showMessage,status : false})}>
                Close
            </Button>            
            </Modal.Footer>

        </Modal>
        </>
    )
}

export default ValidationModal
