import React,{useState} from 'react'
import {Modal,Button} from "react-bootstrap"
function TermsandConditions() {
    const [show, setShow] = useState(false);
    return (
        <>

        <span 
            className="text-secondary " 
            style={{borderBottom: "dashed", cursor:"pointer"}}
            onClick = {()=>setShow(true)}
        >
            Terms and Conditions
        </span>

        <Modal
            show={show}
            onHide={()=>setShow(false)}
            backdrop="static"
            keyboard={false}
        >

            <Modal.Header closeButton>
            <Modal.Title>Terms and Condition</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                Ipsum molestiae natus adipisci modi eligendi? Debitis amet quae unde
                commodi aspernatur enim, consectetur. Cumque deleniti temporibus
                ipsam atque a dolores quisquam quisquam adipisci possimus
                laboriosam. Quibusdam facilis doloribus debitis! Sit quasi quod
                accusamus eos quod. Ab quos consequuntur eaque quo rem! Mollitia
                reiciendis porro quo magni incidunt dolore amet atque facilis ipsum
                deleniti rem!
            </Modal.Body>

            <Modal.Footer>
            <Button variant="secondary" onClick={()=>setShow(false)}>
                Close
            </Button>            
            </Modal.Footer>

        </Modal>
        </>
    )
}

export default TermsandConditions
