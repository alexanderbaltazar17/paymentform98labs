import React from 'react'
function CreditCard({paymentDetails, handleCard}) {

    return (
        <>           
        <div className="row mt-2" id="creditcardtab">   
                      
            <div className="col-md-6 p-4" style={{border:"1px solid #eee"}}>
                
                    <div className="input-group mb-3">
                        <label className="font-weight-bold col-12 p-0">Card Number <span className="text-danger">*</span></label>
                        <input type="text" 
                            className="form-control col-12"                                     
                            aria-describedby="basic-addon2"
                            value= {paymentDetails.cardnumber}
                            name= "cardnumber"
                            id="cardnumber"
                            onChange = {handleCard}
                        />
                        <div className="input-group-append">
                            <span className="input-group-text" id="basic-addon2">
                                <svg width="3em" height="1em" viewBox="0 0 16 16" className="bi bi-credit-card" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path fillRule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v1h14V4a1 1 0 0 0-1-1H2zm13 4H1v5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V7z"/>
                                    <path d="M2 10a1 1 0 0 1 1-1h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1z"/>
                                </svg>
                            </span>                                    
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="font-weight-bold col-12 p-0">Valid thru(mm/yy) <span className="text-danger">*</span></label>
                        <div className="row">
                            <div className="col-4">
                                <input 
                                    className = "form-control"
                                    type="text"
                                    name="mm" 
                                    value={paymentDetails.mm}
                                    onChange = {handleCard}                                                                         
                                />
                            </div>
                                <span className="mt-1 font-weight-bold">/</span>
                            <div className="col-4">
                                <input 
                                    className = "form-control"
                                    type="text"
                                    name="yy" 
                                    value={paymentDetails.yy}
                                    onChange = {handleCard}                                                                         
                                />
                            </div>                                         
                        </div>                                                           
                    </div>

                    <div className="form-group">
                        <label className="font-weight-bold col-12 p-0">Cardholder's Name <span className="text-danger">*</span></label>
                        <input 
                            className = "form-control"
                            type="text"
                            name = "owner"
                            value={paymentDetails.owner}
                            onChange = {handleCard}
                        />                                
                    </div>                            
            </div>
                
            <div className="col-md-6 p-4">
                <div className="input-group mb-3">
                    <label className="font-weight-bold col-12 p-0">CVV/CVC <span className="text-danger">*</span></label>
                    <input type="text" 
                        className="form-control col-3"
                        name="cvc" 
                        value={paymentDetails.cvc}
                        onChange = {handleCard}
                        aria-describedby="basic-addon2"/>
                    <div className="input-group-append" >
                        <span className="input-group-text font-weight-bold" id="basic-addon2">
                            ?
                        </span>                                    
                    </div>
                </div>                 
            </div>

        </div>          
        </>
    )
}

export default CreditCard
