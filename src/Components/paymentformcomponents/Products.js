import React from 'react'

function Products({product}) {
    return (
        <>
            <div className="row">
                <div className="col-8">
                    <p className="ml-2"><a href="#" dangerouslySetInnerHTML={{ __html: product.name }}></a></p>
                </div>
                <div className="col-4">
                    <p className="text-center">${product.price}</p>
                </div>
            </div>
        </>
    )
}

export default Products
