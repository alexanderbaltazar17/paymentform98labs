import React,{useState} from 'react'
import Products from "./paymentformcomponents/Products"
import ShippingMethods from "./paymentformcomponents/ShippingMethods"
import PaymentMethods from "./paymentformcomponents/PaymentMethods"
import CreditCard from "./paymentformcomponents/paymentmethodform/CreditCard"
import TNC from "./paymentformcomponents/paymentmethodform/TermsandConditions"
import ValidationModal from "./paymentformcomponents/ValidationModal"

function PaymentForm() {

    const [products] = useState([
        {
            key : 1,
            name : `Apple &reg; iPad&reg; with Retina&reg; display Wi-Fi - 32gb - White`,
            price : 499.00
        },
        {
            key : 2,
            name : `16GB A Series Walkman Video MP3`,
            price : 130.00
        }
    ]);

    const [shippingMethods] = useState ([
        {
            key : 1,
            name : "FedEx",
            price : 13.99
        }
    ])

    const [activeTab, setActiveTab] = useState([
        {
            card : "Credit Card", 
            status : true
        },
        {
            card : "Gift Card",
            status : false
        },        
        {
            card : "PayPal",
            status : false
        }
    ])    

    const ProductList = products.map(product => {
        return (
            <Products 
                key = {product.key}
                product = {product}
            />
        )
    })

    const ShippingMethodList = shippingMethods.map(method => {
        return (
            <ShippingMethods 
                key = {method.key}
                product = {method}
            />
        )
    })    
    
    const changeTab = (newtab) => {                
        var updatedTab = activeTab.map(list => {            
            if(list.card === newtab.card){
                if(newtab.status === false){
                    return {                        
                        card : list.card,
                        status: !list.status
                    }
                }
                else{
                    return {                        
                        card : list.card,
                        status : list.status
                    }
                }
            }
            else{
                return {                        
                    card : list.card,
                    status : false
                }
            }                                                 
        })        
        setActiveTab(updatedTab);        
    }

    const PaymentMethodList = activeTab.map( (tab,index) => {
        return (
            <PaymentMethods 
                key={tab.card} 
                tab = {tab}
                handleActive = {changeTab}
                order = {index === 0? true : false}                
            />
        )        
    } ) 
    
    const [paymentDetails,setPaymentDetails] = useState({        
        cardnumber : '',
        cvc : '',
        mm : '',
        yy : '',
        owner : '',
        isAccepted : false        
    })    

    const handleCard = (event) => {
        const {name, value, type} = event.target
        const digits = /^[0-9 \b]+$/;           

               
        if( (name === "cardnumber")){ //if not black, also removed spaces before checking             
            if(digits.test(value.replace(/\s+/g, "")))
            {
                var cc = "";
                [...value.replace(/\s+/g, "")].forEach(function (val, i) {
                    cc += (i > 0 && i % 4 === 0) ? " "+val : val
                });                            
                setPaymentDetails({
                    ...paymentDetails,
                    [name] : cc
                })
            }                        
        }

        else if(name === "cvc"){
            if(digits.test(value)){
                var newCVC = "";
                [...value.replace(/\s+/g, "")].forEach(function (val, i) {
                    if (i < 3) newCVC+=val;
                });
                setPaymentDetails({
                    ...paymentDetails,
                    [name] : newCVC
                })
            }            
        }
        else if( (name === "mm" || name ==="yy") && digits.test(value) === true){
            var my = "";
            [...value.replace(/\s+/g, "")].forEach(function (val, i) {
                if (i < 2) my+=val;
            });
            setPaymentDetails({
                ...paymentDetails,
                [name] : my
            })
        }
        else{               
            if(type === "checkbox"){
                setPaymentDetails({
                    ...paymentDetails,
                    [name] : event.target.checked
                })
            }else{
                setPaymentDetails({
                    ...paymentDetails,
                    [name] : value
                })
            }                      
        }        
    }

    const opentab = activeTab.map( (list,i) =>{
        switch(list.card){
            case "Credit Card" : 
                if(list.status === true){
                    return (
                        <CreditCard key={i} paymentDetails = {paymentDetails} handleCard = {handleCard}/>
                    )
                }else{
                    return null
                }
        }
    })

    const [showMessage,setShowMessage] = useState({
        status: false,
        message : "",
        title : "",
    })

    const validateForm = (e) => {    
        e.preventDefault();    
        var validated = true;
        if(paymentDetails.cardnumber === "" ||
            paymentDetails.cvc === "" ||
            paymentDetails.mm === "" ||
            paymentDetails.yy === "" ||
            paymentDetails.owner === "" ||
            !paymentDetails.isAccepted )
        {
            validated = false;
        }
        if(!validated){
            setShowMessage({...showMessage,
                status:true,
                message: "Please Fill up the Form",
                title:"Invalid"
            })
        }
        else{
            setShowMessage({...showMessage,
                status:true,
                message: "Success!",
                title:"Success"    
            })
            e.target.submit()
        }
    }

    return (        
        <div id="parentContainer" 
            className="space-around container d-flex p-5
            align-items-start justify-content-center" >            
            
            <form onSubmit={validateForm}>
            <div id= "formcontainer" className="col-10">

                <div className="row">
                    <div className="col d-flex justify-content-center text-muted" style={{height:"70px"}}>
                        <img src="/images/logo192.png" 
                            alt="demo_store_logo" 
                            height="30px"
                            className="mr-2"                            
                        />
                        <span className="font-weight-bold mr-1">DEMO </span> STORE
                    </div>
                </div>

                <div className="row mt-2">
                    <div className="col p-0">
                        <h2>Checkout</h2>
                    </div>
                </div>                            

                <div className="row mt-2" id="products">                
                    <div className="col p-0">
                        <h4 className="font-weight-normal mb-3">Products</h4>                    
                        {ProductList}
                    </div>                    
                </div>

                <div className="row mt-2" id="shippingmethod">                
                    <div className="col p-0">
                        <h4 className="font-weight-normal mb-3">Shipping Method</h4>
                        {ShippingMethodList}
                    </div>                    
                </div>

                <div className="row mt-2" id="paymentmethod">                
                    <div className="col p-0">
                        <h4 className="font-weight-normal mb-3">Payment Method</h4>
                        <div className="row">
                            <div className="col d-flex flex-wrap justify-content-between">    
                                {PaymentMethodList}
                            </div>              
                        </div>                                                                
                    </div>                    
                </div>

                {opentab}
                                                  
                <div className="row mt-3" id="termsandconditions">                
                    <div className="col p-2" style={{border:"1px solid #eee",boxShadow:"0px 0px 2px 0px"}}> 
                        
                    <div className="col d-flex">
                        <div className="form-check">
                            <input 
                                className="form-check-input" 
                                type="checkbox"
                                id="accepted"
                                name = "isAccepted"
                                checked = {paymentDetails.isAccepted}
                                onChange = {handleCard}
                                />
                            <label className="form-check-label" htmlFor="defaultCheck1">
                                I accept the 
                            </label>
                        </div>
                        <div className="ml-1">
                            <TNC/>                            
                        </div>
                    </div>    
                                        
                    </div>
                </div>

                <div className="row mt-3" id="checkoutbutton">                
                    <button 
                        className="col p-3 text-center border-0" 
                        style={{backgroundColor:"#ff5319",color:"#fff",cursor:"pointer"}}                          
                    > 
                        Place Order ( $624.99 )
                    </button>                
                </div>

                <ValidationModal showMessage= {showMessage} setShowMessage = {setShowMessage}/>
            </div>{/* end of formcontainer */}
            </form>
        </div>// end of parentcontainer
    )
}

export default PaymentForm
